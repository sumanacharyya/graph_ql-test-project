import React, { useState } from 'react'
import { useQuery, useLazyQuery, gql, useMutation } from '@apollo/client';


const QUERY_All_USERS = gql`
    query GetAllUsers{
        users{
            id
            name
            username
            age
            nationality
        }
    }
`

const QUERY_All_MOVIES = gql`
    query GetAllMovies{
        movies{
            id
            name
            yearOfPublication
            isInTheaters
        }
    }
`

const QUERY_MOVIE_BY_NAME = gql`
    query Movie($name: String!){
        movie(name: $name){
            id
            name
            yearOfPublication
            isInTheaters
        }
    }
`

const CREATE_USER_MUTATION = gql`
    mutation CreateUser($input: CreateUserInput!) {
        createUser(input: $input){
            id
            name
            username
        }
    }
`

const DisplayData = () => {
    const [searchMovie, setSearchMovie] = useState("");

    // Create Users Satte
    const [name, setName] = useState("");
    const [username, setUsername] = useState("");
    const [age, setAge] = useState(null);
    const [nationality, setNationality] = useState("");
    
    const {data, loading, refetch, error} = useQuery(QUERY_All_USERS);
    const {data: movieDaat, loading: movieLoading, error: movieErr} = useQuery(QUERY_All_MOVIES);
    const [fetchMovie, {
        data: fetchedSearchedMovie, error: fetchedSearchedMovieError,
    }] = useLazyQuery(QUERY_MOVIE_BY_NAME);

    const [createUser] = useMutation(CREATE_USER_MUTATION);
    

    if(loading){
        return <h1>LOADING PLEASE WAIT...</h1>
    }
    
    if(error){
        return <h1>ERROR WHILE FETCHING...</h1>
    }
    
  return (
    <>
        <div>
            <input type="text" placeholder='Enter Name' onChange={(e) => setName(e.target.value)} />
            <input type="text" placeholder='Enter Username' onChange={(e) => setUsername(e.target.value)} />
            <input type="number" placeholder='Enter Age' value={age} onChange={(e) => setAge(Number(e.target.value))} />
            <input type="text" placeholder='Enter Nationality' onChange={(e) => setNationality(e.target.value.toUpperCase())} />
            <button onClick={() => {createUser({
                variables: {
                    input: {
                        name,
                        username,
                        age,
                        nationality,
                    }
                }
            });
            refetch();
            }}>Create User</button>
        </div>
        <div>Display User Data</div> <br /> 
        {
            data?.users.length > 0 && !error && data.users.map((user) => (
                <div key={user.id}>
                    <h3>{user.name}</h3>
                    <h3>{user.username}</h3>
                    <h4>{user.age}</h4>
                    <h4>{user.nationality}</h4>
                </div>
            ))
        }
        <hr />
        <div>Display Movie Data</div> <br />
        <div>
            <input type="text" placeholder='Enter Movie Name' onChange={(e) => {setSearchMovie(e.target.value)}} />
            <button onClick={() => fetchMovie({variables: { name: searchMovie, }})}>Search Movie</button>
            <div>
                {
                    fetchedSearchedMovie && 
                    <div>
                        <h2>Movie Name : {fetchedSearchedMovie.movie.name}</h2>
                        <h4>Year Of Publication : {fetchedSearchedMovie.movie.yearOfPublication}</h4>
                    </div>
                }

                {
                    fetchedSearchedMovieError && <h1>ERROR ON SEARCHED FETCHING RESULT...</h1>
                }
            </div>
        </div> 

        {
            movieDaat?.movies.length > 0 && !movieErr && movieDaat.movies.map((movie) => (
                <div key={movie.id}>
                    <h3>{movie.name}</h3>
                    <h3>{movie.yearOfPublication}</h3>
                    <h4>{movie.isInTheaters}</h4>
                </div>
            ))
        }
    </>
  )
}

export default DisplayData;