const  UserList = [
    {
        id: 1,
        name: "Suman Acharyya",
        username: "sumanac",
        age: 20,
        nationality: "INDIAN",
        friends: [
            {
                id: 3,
                name: "Demi Kateya",
                username: "sumanac",
                age: 20,
                nationality: "BELGIUM",
            },
            {
                id: 4,
                name: "Shinjo Shimaro",
                username: "sumanac",
                age: 20,
                nationality: "JAPANESE",
            },
        ]
    },
    {
        id: 2,
        name: "Sanchari San",
        username: "sumanac",
        age: 20,
        nationality: "INDIAN",
    },
    {
        id: 3,
        name: "Demi Kateya",
        username: "sumanac",
        age: 20,
        nationality: "BELGIUM",
        friends: [
            {
                id: 5,
                name: "Dan Travis",
                username: "sumanac",
                age: 20,
                nationality: "AMERICAN",
            },
            {
                id: 6,
                name: "Harry Luke",
                username: "sumanac",
                age: 20,
                nationality: "DUTCH",
            },
        ]
    },
    {
        id: 4,
        name: "Shinjo Shimaro",
        username: "sumanac",
        age: 20,
        nationality: "JAPANESE",
    },
    {
        id: 5,
        name: "Dan Travis",
        username: "sumanac",
        age: 20,
        nationality: "AMERICAN",
    },
    {
        id: 6,
        name: "Harry Luke",
        username: "sumanac",
        age: 20,
        nationality: "DUTCH",
    },
];

const MovieList = [
    {
        id: 1,
        name: "Abcd D",
        yearOfPublication: 2018,
        isInTheaters: true,
    },
    {
        id: 2,
        name: "Abc De",
        yearOfPublication: 2021,
        isInTheaters: true,
    },
    {
        id: 3,
        name: "Abcdsdsd Df",
        yearOfPublication: 2020,
        isInTheaters: true,
    },
    {
        id: 4,
        name: "Asdhbgbcd",
        yearOfPublication: 2012,
        isInTheaters: true,
    },
    {
        id: 5,
        name: "Abbcxvbcd",
        yearOfPublication: 2023,
        isInTheaters: false,
    },
    {
        id: 6,
        name: "Abcsasasad D",
        yearOfPublication: 2018,
        isInTheaters: false,
    },
]

module.exports = {UserList, MovieList};