const { ApolloServer } = require("apollo-server");
// const { isRequiredArgument } = require("graphql");
// import {typeDefs} from './schema/typeDefs.js'
// const {typeDefs} = require('./')
const {typeDefs} = require("./schema/type-defs");
const {resolvers} = require("./schema/resolvers");

const Port  = process.env.PORT || 4000;

const server = new ApolloServer({
    typeDefs, resolvers, context: ({req}) => {
        return {req};
    }
});


server.listen(Port).then(({url}) => {
    console.log(`Running on port : ${url}`);
})